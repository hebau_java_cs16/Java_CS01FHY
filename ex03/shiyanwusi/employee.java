import java.util.Date;

import java.util.Comparator;
public class employee {
	private String no;
	private String name;
	private String sex;
	private Date birthday;
	private Bumen department;
	private Date date;
	public employee(){}
	public employee(String no,String name,String sex){
		this.no=no;
		this.name=name;
		this.sex=sex;
	}
	private Bumen bumen;
	public Bumen getBumen(){
		return bumen;
	}
	public void setBumen(Bumen bumen){
		this.bumen=bumen;
	}
	public String getNo(){
		return no;
	}
	public String getName(){
		return name;
	}
	public String getSex(){
		return sex;
	}
	public Date getBirthday(){
		return birthday;
	}
	public Bumen getDepartment(){
		return department;
	}
	public Date getDate(){
		return date;
	}
	public void setNo(String no){
		this.no=no;
	}
	public void setName(String name){
		this.name=name;
	}
	public void setSex(String sex){
		this.sex=sex;
	}
	public void setBirthday(Date birthday){
		this.birthday=birthday;
	}
	public void setDepartment(Bumen department){
		this.department=department;
	}
	public void setDate(Date date){
		this.date=date;
	}
	public String toString(){
		return "职工号:"+this.no+"\t姓名:"+this.name+"\t性别:"+this.sex+"\t生日:"+this.birthday+"\t工作部门:"+this.department+"\t入职时间:"+this.date;
	}
	public int compareTo(employee o){
		if(this.getBirthday().getTime()>o.getBirthday().getTime()){
			return 1;
		}
		if(this.getBirthday().getTime()<o.getBirthday().getTime()){
			return -1;
		}
		else{
			return 0;
		}
	
	}
	class employeeComparator implements Comparator<employee>{
		public int compare(employee o1,employee o2){
			if(o1.getBirthday().getTime()>o2.getBirthday().getTime()){
				return 1;
			}
			if(o1.getBirthday().getTime()<o2.getBirthday().getTime()){
				return -1;
			}
			else{
				return 0;
			}
		}
	}
}
