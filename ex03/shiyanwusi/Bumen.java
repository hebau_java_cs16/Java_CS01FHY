
public class Bumen {
	private String bno;
	private String bname;
	private employee manager;
	public Bumen(String bno,String bname){
		this.bno=bno;
		this.bname=bname;
	}
	public void setBno(String bno){
		this.bno=bno;
	}
	public void setBname(String bname){
		this.bname=bname;
	}
	public void setManager(employee manager){
		this.manager=manager;
	}
	public String getBno(){
		return bno;
	}
	public String getBname(){
		return bname;
	}
	public employee getmanager(){
		return manager;
	}
	public String toString(){
		return"部门编号："+this.bno+"\n部门名称:"+this.bname+"\n经理:"+this.manager;
	}
}
