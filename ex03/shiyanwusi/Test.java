
import java.util.Scanner;
import java.util.Arrays;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {
	public static void main(String[] args) throws Exception {
		System.out.println("\t\t\t员工查询系统");
		System.out.println("1.后勤部");
		System.out.println("2.调查部");
		Scanner input = new Scanner(System.in);
		System.out.println("请输入您要查询的部门");
		int i=input.nextInt();
		SimpleDateFormat S=new SimpleDateFormat("yyyy-MM-dd");
		Bumen bm[]=new Bumen[2];
		bm[0]=new Bumen("001","后勤部");
		bm[1]=new Bumen("002","调查部");
		
		employee em1[]=new employee[5];
		employee em2[]=new employee[5];
		em1[0]=new employee("1601","张一","男");
		em2[0]=new employee("1602","张立","男");
		em1[1]=new employee("1603","王氏","男");
		em1[2]=new employee("1604","王三","男");
		em2[1]=new employee("1605","李丹","女");
		em1[3]=new employee("1606","赵武","男");
		em2[2]=new employee("1607","杨六","男");
		em1[4]=new employee("1608","钱多","男");
		em2[3]=new employee("1609","孙立","男");
		em2[4]=new employee("1610","张倩","女");
		
		bm[0].setManager(em1[0]);
		bm[1].setManager(em2[0]);
		em1[0].setBumen(bm[0]);
		em2[0].setBumen(bm[1]);
		em1[1].setBumen(bm[0]);
		em1[2].setBumen(bm[0]);
		em2[1].setBumen(bm[1]);
		em1[3].setBumen(bm[0]);
		em2[3].setBumen(bm[1]);
		em1[4].setBumen(bm[0]);
		em2[3].setBumen(bm[1]);
		em2[4].setBumen(bm[1]);
		
		Date birthday1[]={S.parse("1988-01-20"),
					S.parse("1970-05-18"),
					S.parse("1968-10-24"),
					S.parse("1974-07-25"),
					S.parse("1983-04-20")};
		Date birthday2[]={
					S.parse("1976-06-18"),
					S.parse("1978-01-14"),
					S.parse("1957-06-24"),
					S.parse("1987-01-02"),
					S.parse("1960-02-07"),
		};
		Date date1[]={S.parse("2008-10-20"),
				S.parse("1990-07-18"),
				S.parse("1990-01-24"),
				S.parse("1996-03-02"),
				S.parse("2005-04-04")};
		Date date2[]={
				S.parse("2000-06-18"),
				S.parse("1999-10-14"),
				S.parse("1988-04-04"),
				S.parse("2015-10-02"),
				S.parse("1996-02-17")
		};
		for(int m=0;m<em1.length;m++){
			em1[m].setBirthday(birthday1[m]);;
		}
		for(int m=0;m<em2.length;m++){
			em2[m].setBirthday(birthday2[m]);;
		}
		for(int n=0;n<em1.length;n++){
			em1[n].setDate(date1[n]);
		}
		for(int n=0;n<em2.length;n++){
			em2[n].setDate(date2[n]);
		}
		Arrays.sort(birthday1);
		Arrays.sort(birthday2);
		if(i==1){
			System.out.println(bm[0].toString());
	  		System.out.println(bm[0].getBname()+"员工：");
	  		for(int i1=0;i1<em1.length;i1++){
	  			System.out.println(em1[i1].toString());
	  		}
		}
		else{
			System.out.println(bm[1].toString());
			System.out.println(bm[1].getBname()+"员工：");
			for(int i2=0;i2<em2.length;i2++){
	  			System.out.println(em2.toString());
	  		}
		
		
		}
		
		
  		
	}

}
