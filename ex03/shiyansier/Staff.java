package 实验四二;

class Staff {
	private String name;
	private int age;
	private String sex;
	public Staff(){}
	public Staff(String name,int age,String sex){
		this.name=name;
		this.age=age;
		this.sex=sex;
	}
	public String getName(){
		return name;
	}
	public int getAge(){
		return age;
	}
	public String getSex(){
		return sex;
	}
	public void setName(String name){
		this.name=name;
	}
	public void setAge(int age){
		this.age=age;
	}
	public void setSex(String sex){
		this.sex=sex;
	}
	public String toString(){
		return "姓名："+this.name+"年龄"+this.age+"性别"+this.sex;
	}
}
