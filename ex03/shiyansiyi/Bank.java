package bank;
public class Bank {
		private static String bankName;
		private String name;
		private String password;
		private double balance;
		private double turnover;
		public static void welcome(){
			System.out.println("欢迎来到建设银行");
		}
		public Bank(){
		}
		public Bank(String name,String password,double balance){
			this.name=name;
			this.password=password;
			this.balance=(balance-10);
		}
		public double getbalance(){
			return balance;
		}
		public void setbalance(double balance){
			this.balance=balance;
		}
		public String getpassword(){
			return password;
		}
		public void setpassword(String password){
			this.password=password;
		}
		public double getturnover(){
			return turnover;
		}
		public void settrunover(double turnover){
			this.turnover=turnover;
		}
		public void deposit(double a){
			balance=getbalance()+a;
			setbalance(balance);
			System.out.println("您现在的账户余额为"+balance);
		}
		public void withdrawal(String str1,double turnover){
			//System.out.println("请输入取款密码");
			//str1=input.next();
			String s=getpassword();
			//System.out.println(s);
			if(str1.equalsIgnoreCase(s)==false){
				System.out.println("密码错误，请重新输入");
			}
			else if(turnover>getbalance()){
				System.out.println("余额不足，不能办理业务");
			}
			else{
				balance=getbalance()-turnover;
				System.out.println("您好，您的账户已经取出"+turnover+"元，当前账户余额为"+balance);
			}
		}
		public void welcomeNext(){
			System.out.println("欢迎下次光临");
		}
}