package 实验五三;

public class SendTask {
	private String danhao;
	private double heavy;
	public SendTask(){}
	public SendTask(String danhao,double heavy){
		this.danhao=danhao;
		this.heavy=heavy;
	}
	public void setDanhao(String danhao){
		this.danhao=danhao;
	}
	public String getDanhao(){
		return danhao;
	}
	public void setHeavy(double heavy){
		this.heavy=heavy;
	}
	public double getHeavy(){
		return heavy;
	}
	public void sendBefore(){
		System.out.println("订单开始处理，仓库开始验货。。。\n"+"快递单号"+this.danhao+"\n"+"货物重量"+this.heavy+"kg"+"\n"+"订单已发货\n");
	}
	public void send(Transportantion t,Gps tool){
		JDTransportation t1=new JDTransportation();
		Phone phone=new Phone();
		System.out.println("运货人"+t.getPeople()+"正在驾驶车辆编号为"+t.getNo()+"的"+t.getXinghao()+"型号的车发送货物！\n"+t1.transport()+"货物当前坐标："+phone.showCoordinate()+"\n");
	}
	public void sendAfter(Transportantion t){
		System.out.println("货物已送到，等待签收");
	}
}
