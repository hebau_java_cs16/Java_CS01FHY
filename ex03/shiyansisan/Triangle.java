package pingmian;

public class Triangle extends Pingmian {
	int a,b,c;
	Triangle(){}
	Triangle(int a,int b,int c){
		this.a=a;
		this.b=b;
		this.c=c;
	}
	public void setA(int a){
		this.a=a;
	}
	public void setB(int b){
		this.b=b;
	}
	public void setC(int c){
		this.c=c;
	}
	public int getA(){
		return a;
	}
	public int getB(){
		return b;
	}
	public int getc(){
		return c;
	}
	public double getC(){
		return (a+b+c);
	}
	public double getS(){
		return ((1.0/4.0)*Math.sqrt((a+b+c)*(a+b-c)*(a+c-b)*(b+c-a)));
	}
	public String toString(){
		return "三角形的面积为："+this.getS()+"三角形的周长为："+this.getC();
	}
}
