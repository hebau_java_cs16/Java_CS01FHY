package pingmian;

public class Boll extends Liti {
	int r;
	Boll(){}
	Boll(int r){
		this.r=r;
	}
	public void setR(int r){
		this.r=r;
	}
	public int getR(){
		return r;
	}
	public double getS(){
		return (4*3.14*r*r);
	}
	public double getV(){
		return ((4.0/3.0)*3.14*r*r*r);
	}
	public String toString(){
		return "球的表面积为："+this.getS()+"球的体积为："+this.getV();
	}
}
