package pingmian;
import java.util.Random;
import java.util.Scanner;
public class Test {
	public static void main(String[] args) {
		Random rand=new Random();
		Scanner input=new Scanner(System.in);
		
		System.out.println("下面将给出圆的半径，请输入出它的面积和周长");
		Yuanxing y=new Yuanxing();
		int r=rand.nextInt(10);
		System.out.println(r);
		y.setR(r);
		double s1=input.nextDouble();
		double c1=input.nextDouble();
		if(s1==y.getS()&&c1==y.getC()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+y.toString());
		}
		
		System.out.println("下面将给出矩形的边长，请输入它的面积和周长");
		Rectangle ra=new Rectangle();
		int a=rand.nextInt(10);
		int b=rand.nextInt(10);
		System.out.println("a="+a);
		System.out.println("b="+b);
		ra.setA(a);
		ra.setB(b);
		double s2=input.nextDouble();
		double c2=input.nextDouble();
		if(s2==ra.getS()&&c2==ra.getC()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+ra.toString());
		}
		
		System.out.println("下面将给出三角形的边长，请输入它的面积和周长");
		Triangle rb=new Triangle();
		int a1=rand.nextInt(10);
		int b1=rand.nextInt(10);
		int c=rand.nextInt(10);
		System.out.println("a="+a);
		System.out.println("b="+b);
		System.out.println("c="+c);
		rb.setA(a);
		rb.setB(b);
		rb.setC(c);
		double s3=input.nextDouble();
		double c3=input.nextDouble();
		if(s3==ra.getS()&&c3==ra.getC()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+rb.toString());
		}
		
		System.out.println("下面将给出球的半径，请输入出它的表面积和体积");
		Boll ba=new Boll();
		int r1=rand.nextInt(4);
		System.out.println(r1);
		ba.setR(r1);
		double s4=input.nextDouble();
		double v1=input.nextDouble();
		if(s4==ba.getS()&&v1==ba.getV()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+ba.toString());
		}
		
		
		System.out.println("下面将给出圆柱的半径和高，请输入出它的表面积和体积");
		Cylinder d=new Cylinder();
		int r2=rand.nextInt(10);
		int h=rand.nextInt(10);
		System.out.println("r="+r2);
		System.out.println("h="+h);
		d.setR(r2);
		d.setH(h);
		double s5=input.nextDouble();
		double v2=input.nextDouble();
		if(s5==d.getS()&&v2==d.getV()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+d.toString());
		}
		
		System.out.println("下面将给出圆锥的半径、高、母线长，请输入出它的表面积和体积");
		Cone e=new Cone();
		int r3=rand.nextInt(10);
		int h1=rand.nextInt(10);
		int l=rand.nextInt(10);
		System.out.println("r="+r3);
		System.out.println("h="+h1);
		System.out.println("l="+l);
		e.setR(r3);
		e.setH(h1);
		e.setL(l);
		double s6=input.nextDouble();
		double v3=input.nextDouble();
		if(s6==e.getS()&&v3==e.getV()){
			System.out.println("回答正确");
		}
		else{
			System.out.println("回答错误，正确答案:"+e.toString());
		}
	}

}
