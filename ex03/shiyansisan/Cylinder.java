package pingmian;

public class Cylinder extends Liti {
	int r,h;
	Cylinder(){}
	Cylinder(int r,int h){
		this.r=r;
		this.h=h;
	}
	public void setR(int r){
		this.r=r;
	}
	public int getR(){
		return r;
	}
	public void setH(int h){
		this.h=h;
	}
	public int getH(){
		return h;
	}
	public double getS(){
		return (h*2*3.14*r+3.14*r*r*2);
	}
	public double getV(){
		return (3.14*r*r*h);
	}
	public String toString(){
		return "圆柱的表面积为："+this.getS()+"圆柱的体积为："+this.getV();
	}
}
