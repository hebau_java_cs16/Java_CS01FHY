package pingmian;

public class Cone extends Liti {
	int r,h,l;
	Cone(){}
	Cone(int r,int h,int l){
		this.r=r;
		this.h=h;
		this.l=l;
	}
	public void setR(int r){
		this.r=r;
	}
	public int getR(){
		return r;
	}
	public void setH(int h){
		this.h=h;
	}
	public int getH(){
		return h;
	}
	public void setL(int l){
		this.l=l;
	}
	public int getL(){
		return l;
	}
	public double getV(){
		return ((1.0/3.0)*3.14*r*r*h);
	}
	public double getS(){
		return (3.14*r*l+3.14*r*r);
	}
	public String toString(){
		return "圆锥的表面积为："+this.getS()+"圆锥的体积为："+this.getV();
	}
}
