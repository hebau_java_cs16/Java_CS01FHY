package 实验五一;

public class Dog implements Pet{
	private String no;
	private String kind;//种类
	private String breed;//品种
	private int price;
	private int num;//数量
	private int number;
	public Dog(String no,String kind,String breed,int price,int num){
		this.no=no;
		this.kind=kind;
		this.breed=breed;
		this.price=price;
		this.num=num;
	}
	public void setNo(String no){
		this.no=no;
	}
	public void setKind(String kind){
		this.kind=kind;
	}
	public void setBreed(String breed){
		this.breed=breed;
	}
	public void setPrice(int price){
		this.price=price;
	}
	public void setNum(int num){
		this.num=num;
	}
	public String getNo(){
		return no;
	}
	public String getKind(){
		return kind;
	}
	public String getBreed(){
		return breed;
	}
	public int getPrice(){
		return price;
	}
	public int getNum(){
		return num;
	}
	public void setNumber(int number){
		this.number=number;
	}
	public  int getNumber(){
		return number;
	}
}
