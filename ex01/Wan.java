import java.util.Scanner;
public class Wan{
	public static void main(String args[]){
		System.out.println("请输入您要判断的年份!");
		Scanner input=new Scanner(System.in);
		int year=input.nextInt();
		if( isLeap(year)==true){
			System.out.println(year+"年是闰年！");
		}
		else{
			System.out.println(year+"年是平年！");
		}
		
		System.out.println("请输入您要判断的年份和月份!");
		
		int day=input.nextInt();
		int month=input.nextInt();
		
		if(days(year,month)==31){
			System.out.println("该月有31天！");
		}
		else if(days(year,month)==30){
			System.out.println("该月有30天！");
		}
		else if(days(year,month)==29){
			System.out.println("该月有29天！");
		}
		else if(days(year,month)==28){
			System.out.println("该月有28天！");
		}
		
		System.out.println("请输入距离1900年1月1日您要计算的年份和月份!");
		year=input.nextInt();
		month=input.nextInt();
		System.out.println("距离1900年1月1日的天数为"+totalDays(year,month));
		
		System.out.println("您想打印哪年哪月的日历？请输入年份和月份");
		year=input.nextInt();
		month=input.nextInt();
		print(year,month);
  }
	
public static boolean isLeap(int year){
		if((year%4==0&&year%100!=0)||(year%400==0)){
			boolean flag=true;
			return flag;
		}
		else{
			return false;
		}
	}
public static int days (int year,int month){
	if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
		return 31;
	}
	else if(month==4||month==6||month==9||month==11){
		return 30;
	}
	if(((year%4==0&&year%100!=0)||(year%400==0))){
		return 29;
	}
	else{
		return 28;
	}
}

public static int totalDays(int year,int month){
	int i,j,count,sum1=0,sum2=0;
	for(i=1900;i<year;i++){
		if((i%4==0&&i%100!=0)||(i%400==0)){
			sum1+=366;
		}
		else{
			sum1+=365;
		}
	}
	for(j=1;j<month;j++){
		if(j==1||j==3||j==5||j==7||j==8||j==10||j==12){
			sum2+=31;
		}
		else if(j==4||j==6||j==9||j==11){
			sum2+=30;
		}
		else{
			i=year;
			if((i%4==0&&i%100!=0)||(i%400==0)&&i==2){
				sum2+=29;
			}
			else{
				sum2+=28;
			}
		}	
	}
	count=sum1+sum2;
	return count;
}
public static void print(int year, int month){
	int sum,day,i,j,count;
	sum=totalDays(year,month);
	count=1+(sum%7);
	System.out.printf("%d年%d月\n",year,month);
	System.out.println("星期日\t星期一\t星期二\t星期三\t星期四\t星期五\t星期六\n");
	if(month==4||month==6||month==9||month==11)       
	{
		day=30;
	}
	else if(month==1||month==3||month==5||month==7||month==8||month==10||month==12)      
	{
		day=31;
	}
	else if(((year%4==0&&year%100!=0)||(year%400==0))&&month==2)      
	{
		day=29;
	}
	else      
	{
		day=28;
	}
	for(i=0;i<count;i++)
	{
		System.out.print("\t");
	}
	for(j=1;j<=day;j++)
	{
		System.out.printf("%d\t",j);
		if((j+count)%7==0)
		{
			System.out.print("\n");				
		}
	}
}
}

