package frequency;
import java.util.Scanner;
public class frequency {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("请输入字符串");
		String str1=input.next();
		System.out.println("请输入需要查找的字符串");
		String str2=input.next();
		int i=cishu(str1,str2);
		if(i==-1){
			System.out.println("没有查找到该子串");
		}
		else{
			System.out.println("该子串出现次数为"+i+"次");
		}
	}
	public static int cishu(String str1,String str2){
		int i=0,j=0;
		while(str1.indexOf(str2)!=-1){
			j=str1.indexOf(str2);
			str1=str1.substring(j+str2.length(),str1.length());
			i++;
		}
		return i;
	}
}

