import java.util.Scanner;
import java.util.Arrays;
public class Score {
	private int person;
	private double score[];
	public Score(){
	}
	public Score(int person) {
		this.setpeople(person);
	}

	public int getpeople() {
		return person;
	}

	public void setpeople(int n) {
		person = n;
	}
	public double[] getscore() {
		return this.score;
	}

	public void setscore(double score[]) {
		this.score=score;
	}

	public void inputScore() {
		int i,j;
		Scanner input = new Scanner(System.in);
		j=getpeople();
		score=new double[getpeople()];
		for (i = 0; i < j ; i++) {
			
			score[i] = input.nextDouble();
		}
	}
	public double Max( double score[]){
		double max = 0;
		int i;
		for (i = 0; i < score.length; i++) {
				if (max < score[i]) {
					max = score[i];
				}
			}
		return max;
	}
	public double Min(double score[]){
		double min=Max(score);
		int j;
		for (j= 0;j < score.length; j++) {
			if (min > score[j]) {
				min = score[j];
			}
		}
		return min;
	}
	public double avarage(double score[]){
		double ave,s=0,sum=0;
		int x;
		for(x=0;x<score.length;x++){
			s+=score[x];
		}
		sum=s-Min(score)-Max(score);
		
		ave=sum/(score.length-2);
		return ave;
	}

}		
	
