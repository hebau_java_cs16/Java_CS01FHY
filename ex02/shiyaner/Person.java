
 class Person implements Comparable<Person> {
		private String number;
		private String name;
		private double Defen;
        public Person(){            //定义无参构造
        }
		public Person(String name, String number) {       //定义有三个参数的构造方法，为类中的属性初始化
			this.setName(name);
			this.setNumber(number);
			//this.setDefen();
		}

		public String toString() {
			return "姓名： " + name + ",编号" + number+"得分"+Defen+":";
		}

		public String getName() {        //取得姓名
			return name;
		}

		public void setName(String n) {      //设置姓名
			name = n;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String a) {
			number = a;
		}
		public  double getDefen(){
			return Defen;
		}
		public void setDefen(double m){
			Defen=m;
		}

		public int compareTo(Person o) {
			if (this.Defen > o.Defen) {
				return -1;
			} else if (this.Defen < o.Defen) {
				return 1;
			} else {
				return 0;
			}
		}
	}
