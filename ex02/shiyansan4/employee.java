
public class employee {
	private String no;
	private String name;
	private String sex;
	private String birthday;
	private String department;
	private String date;
	public employee(){}
	public employee(String no,String name,String sex,String birthday,String department,String date){
		this.no=no;
		this.name=name;
		this.sex=sex;
		this.birthday=birthday;
		this.department=department;
		this.date=date;
	}
	private Bumen bumen;
	public Bumen getBumen(){
		return bumen;
	}
	public void setBumen(Bumen bumen){
		this.bumen=bumen;
	}
	public String getNo(){
		return no;
	}
	public String getName(){
		return name;
	}
	public String getSex(){
		return sex;
	}
	public String getBirthday(){
		return birthday;
	}
	public String getDepartment(){
		return department;
	}
	public String getDate(){
		return date;
	}
	public void setNo(String no){
		this.no=no;
	}
	public void setName(String name){
		this.name=name;
	}
	public void setSex(String sex){
		this.sex=sex;
	}
	public void setBirthday(String birthday){
		this.birthday=birthday;
	}
	public void setDepartment(String department){
		this.department=department;
	}
	public void setDate(String date){
		this.date=date;
	}
	public String toString(){
		return "职工号:"+this.no+"\t姓名:"+this.name+"\t性别:"+this.sex+"\t生日:"+this.birthday+"\t工作部门:"+this.department+"\t入职时间:"+this.date;
	}
}
